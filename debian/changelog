sublist3r (1.1-4) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.2.
  * debian/copyright: updated packaging copyright years.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Thu, 27 Jul 2023 09:24:29 -0300

sublist3r (1.1-3) unstable; urgency=medium

  * debian/watch: update the search rule to make it compliant with new
    standards of the GitHub.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Sun, 16 Oct 2022 08:25:00 -0300

sublist3r (1.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Mon, 03 Oct 2022 20:33:47 -0300

sublist3r (1.1-1) experimental; urgency=medium

  * Initial release for Debian. (Closes: #1016995)
  * debian/clean: created to remove files generated during build in
    'Sublist3r.egg-info' directory.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped DH level to 13.
      - Bumped Standards-Version to 4.6.1.
      - Improved long description.
      - Set new maintainer and co-maintainer when introducing in Debian.
      - Updated 'Section' field value to 'net'.
      - Updated Vcs-* fields with Debian Security Tools Team Salsa.
  * debian/copyright:
      - Added 'Upstream-Contact' field.
      - Added full license text to GPL-2.
      - Updated main license to GPL-2.
      - Updated packaging copyright years.
      - Updated upstream copyright.
  * debian/gbp.conf: modified to comply with pkg-security team GBP
    configuration.
  * debian/helper-script: removed for not being used anymore.
  * debian/kali-ci.yml: removed because is specific for GitLab in Kali.
  * debian/lintian-overrides: created to override a false positive.
  * debian/manpage/:
      - create-man.sh: created to make the manpage from the .txt source file.
      - sublist3r.1: manpage created using create-man.sh.
      - sublist3r.txt: created to provide the source to the manpage.
  * debian/manpages: added to install the manpage.
  * debian/patches/udpate-usage-info.patch:
      - Added a header to patch.
      - Renamed to 010_update-usage-info.patch.
  * debian/rules: removed not need line with comment.
  * debian/salsa-ci.yml: created to provide CI tests for Salsa.
  * debian/source/lintian-overrides: created to override maintainer-manual-page
    lintian.
  * debian/source/options: removed in favor of a more accurate control via
    'clean' file.
  * debian/sublist3r.docs: renamed to docs.
  * debian/sublist3r.install: removed because not used after deleting the
    helper-script file.
  * debian/tests/control:
      - Added new tests.
      - Removed useless dependency '@' from a test.
  * debian/upstream/metadata: added fields 'Repository' and
    'Repository-Browse'.
  * debian/watch: updated the search rule to make it compliant with new GitHub
    rules.

 -- Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>  Thu, 11 Aug 2022 17:01:49 -0300

sublist3r (1.1-0kali1) kali-dev; urgency=medium

  * New upstream version 1.1
  * Bump Standards-Version to 4.5.1
  * wrap-and-sort

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 07 Apr 2021 17:34:38 +0200

sublist3r (1.0+git20200105-0kali1) kali-dev; urgency=medium

  * New upstream version 1.0+git20200105
  * Use debhelper-compat 12
  * Bump Standards-Version to 4.5.0
  * Refresh patch
  * Add missing info in autopkgtest

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 11 Mar 2020 11:54:32 +0100

sublist3r (1.0+git20181015-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 1.0+git20181015
  * Switch to Python 3
  * Update packaging for new release (use setup.py)
  * Bump Standards-Version to 4.4.0
  * Add a patch to update usage information

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 09 Sep 2019 16:06:45 +0200

sublist3r (1.0+git20170719-0kali1) kali-dev; urgency=medium

  * Initial release (Closes 1964)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 10 Oct 2017 14:22:54 +0200
